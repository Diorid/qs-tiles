# QS-Tiles

QS-Tiles, helps you to control your device easily with custom quick tiles.

[Get on Google Play Store](https://play.google.com/store/apps/details?id=diorid.tiles)

## In-App Features

* Toggle rear flash
* Toggle front flash
* Change between sound modes
* Access to assistant and sound search
* Access to notification history
* Shutdown & reboot device~
* Control immersive mode~~
* Scan QR codes
* Take, edit and share screenshot

you can also take partial screenshot on long click to Screenshot Tile!

~(Needs ROOT permission)

~~(Needs WRITE_SECURE_SETTINGS permission)

## How to Grant Required Permissions?
[Check here](http://www.bit.ly/grantpermission)
## Contributing Ideas
New ideas make a major contribution for QS-Tiles. Therefore if you have something to say about the project feel free to reach us.
## Contributing Bug Reports
If you think that there is a bug, just create new issue. Before doing it please check for the existing issues.
## Contact Us
Please feel free to contact us by mail address if you need any further information.