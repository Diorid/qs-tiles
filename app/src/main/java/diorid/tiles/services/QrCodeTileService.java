package diorid.tiles.services;

import android.content.Intent;
import android.os.Build;
import android.service.quicksettings.Tile;
import android.service.quicksettings.TileService;

import diorid.tiles.utils.QrCodeActivity;

public class QrCodeTileService extends TileService {

    @Override
    public void onClick() {
        super.onClick();

        // launch qr scanner
        Intent i = new Intent(this, QrCodeActivity.class);
        if (Build.VERSION.SDK_INT >= 28) {
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        }
        startActivityAndCollapse(i);

    }

    @Override
    public void onStartListening() {
        super.onStartListening();
        getQsTile().setState(Tile.STATE_INACTIVE);
        getQsTile().updateTile();
    }
}
