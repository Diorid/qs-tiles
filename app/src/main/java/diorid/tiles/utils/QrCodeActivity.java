package diorid.tiles.utils;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.google.zxing.Result;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrCodeActivity extends Activity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CODE = 100;
    public final String URL_REGEX = "^((https?|ftp)://|(www|ftp)\\.)?[a-z0-9-]+(\\.[a-z0-9-]+)+([/?].*)?$";
    private ZXingScannerView mScannerView;

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }

    @Override
    public void handleResult(Result rawResult) {

        String s = rawResult.toString();

        Pattern p = Pattern.compile(URL_REGEX);
        Matcher m = p.matcher(s);
        if (m.find()) {
            // if the result equals a link, opens it on browser
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(String.valueOf(rawResult)));
            startActivity(i);
        } else {
            // if the result equals a plain text, copy it to clipboard and show in a dialog
            AlertDialog dialog = new AlertDialog.Builder(this).create();
            dialog.setTitle("Scanned text");
            dialog.setMessage(s);
            dialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK", (dialog1, which) -> {
                dialog1.dismiss();
                mScannerView.resumeCameraPreview(QrCodeActivity.this);
            });
            dialog.setOnCancelListener(dialog12 -> mScannerView.resumeCameraPreview(QrCodeActivity.this));
            dialog.show();

            ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Scanned text", s);
            Toast.makeText(this, "Scanned text copied to clipboard", Toast.LENGTH_LONG).show();
            Objects.requireNonNull(clipboard).setPrimaryClip(clip);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // check the camera permission which is needed to scan qr codes
        if (checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.CAMERA}, REQUEST_CODE);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NotNull String[] permissions, @NotNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_CODE && grantResults[0] == PackageManager.PERMISSION_DENIED) {
            Toast.makeText(this, "Need camera permission to scan qr code.", Toast.LENGTH_LONG).show();
        }
    }
}