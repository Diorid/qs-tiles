package diorid.tiles.screenshot

import android.app.Activity
import android.os.Bundle

class LongClickTileActivity : Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //FIXME Fix the bug that causes to take partial screenshot on long click to every tile.

        ScreenshotTileService.instance?.let {
            Screenshot.acquireScreenshotPermission(this, it)
        }

    }

    override fun onStart() {
        super.onStart()

        // when user long clicked to tile, take partial screenshot
        Screenshot.getInstance().screenshotPartial(applicationContext)
        finish()
    }
}
